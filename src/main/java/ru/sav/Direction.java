package ru.sav;

public enum Direction {
    LEFT,
    RIGHT,
    UP,
    DOWN
}
