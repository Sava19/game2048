package ru.sav;

import java.util.Comparator;

public class KeyComparator implements Comparator<Key> {


    @Override
    public int compare(Key o1, Key o2) {
        if (o1.getI() == o2.getI()) {
            if (o1.getJ() == o2.getJ()) {
                return 0;
            } else {
                return o1.getJ() - o2.getJ();
            }
        } else {
            return o1.getI() - o2.getI();
        }
    }
}
