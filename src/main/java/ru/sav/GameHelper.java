package ru.sav;

import java.util.ArrayList;
import java.util.List;

public class GameHelper {
    List<Integer> moveAndMergeEqual(List<Integer> list) {
        List<Integer> mergedList = new ArrayList<>(list);
        int initialListSize = list.size();

        //removing nulls from the list
        int i = 0;
        while (i < mergedList.size()) {
            if (mergedList.get(i) == null) {
                mergedList.remove(i);
                continue;
            }
            i++;
        }

        i = 0;
        while (i < mergedList.size() - 1) {
            Integer value = mergedList.get(i);
            Integer nextValue = mergedList.get(i + 1);

            if (value == nextValue) {
                mergedList.set(i, value + nextValue);
                mergedList.remove(i + 1);
            }
            i++;
        }

        //adding nulls in the end
        for (int j = mergedList.size(); j < initialListSize; j++) {
            mergedList.add(null);
        }

        return mergedList;
    }
}
