package ru.sav;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Game2048 implements Game {

    public static int GAME_SIZE = 4;

    GameHelper helper = new GameHelper();
    private Board <Key, Integer> board = new SquareBoard<>(GAME_SIZE);
    Random random = new Random();

//    Game2048(Board board) {
//        this.board = board;
//    }

    @Override
    public void init() {
        List<Integer> initialValues = new ArrayList<>(Collections.nCopies(GAME_SIZE*GAME_SIZE, null));
        board.fillBoard(initialValues);
        try {
            addItem();
            addItem();
        } catch (NotEnoughSpace e) {
            System.out.println("Not enough space");
        }

    }



    @Override
    public boolean canMove() {

        if (board.availableSpace().size() != 0) {
            return true;
        } else {
            for (int i = 1; i < GAME_SIZE - 1; i++) {
                for (int j = 1; j < GAME_SIZE; j++) {
                    Integer currentValue = board.getValue(board.getKey(i, j));

                    for (int k = (i - 1); k < (i + 1); k++) {
                        for (int l = (j - 1); l < (j + 1); l++) {
                            if (i != k && j != l) {
                                Integer nearbyValue = board.getValue(board.getKey(k, l));
                                if (currentValue == nearbyValue) {
                                    return true;
                                }
                            }
                        }
                    }

                }
            }
        }
        return false;
    }

    @Override
    public boolean move(Direction direction) {

        for (int i = 0; i < GAME_SIZE; i++) {
            List<Key> keys;
            List<Integer> values;
            List<Integer> newValues;

            switch (direction) {
                case UP:
                    keys = board.getColumn(i);
                    values = board.getValues(keys);
                    newValues = helper.moveAndMergeEqual(values);
                    break;
                case DOWN:
                    keys = board.getColumn(i);
                    Collections.reverse(keys);
                    values = board.getValues(keys);
                    newValues = helper.moveAndMergeEqual(values);
                    break;
                case LEFT:
                    keys = board.getRow(i);
                    values = board.getValues(keys);
                    newValues = helper.moveAndMergeEqual(values);
                    break;
                case RIGHT:
                    keys = board.getRow(i);
                    Collections.reverse(keys);
                    values = board.getValues(keys);
                    newValues = helper.moveAndMergeEqual(values);
                    break;
                default:
                    keys = null;
                    newValues  = null;
                    break;
            }

            for (int j = 0; j < GAME_SIZE; j++) {
                Key key = keys.get(j);
                Integer value = newValues.get(j);
                board.addItem(key, value);
            }
        }

        if (this.canMove()) {
            try {
                this.addItem();
            } catch (NotEnoughSpace e) {
               return false;
            }
            return true;
        } else {
            return false;
        }

    }

    @Override
    public void addItem() throws NotEnoughSpace {
        if (board.availableSpace().size() == 0) {
            throw new NotEnoughSpace();
        }
        int check = random.nextInt(10);
        Integer newValue;

        //new value will be 2 with probability 90 % and will be 4 with 10%
        if (check == 9) {
            newValue = 4;
        } else {
            newValue = 2;
        }

        List<Key> emptyItems = board.availableSpace();
        int randomIndex = random.nextInt(emptyItems.size());
        Key keyForNewItem = emptyItems.get(randomIndex);

        board.addItem(keyForNewItem, newValue);
    }

    @Override
    public Board getGameBoard() {
        return board;
    }

    @Override
    public boolean hasWin() {
//        for (int i = 0; i < GAME_SIZE; i++) {
//            List<Key> keys = board.getRow(i);
//            List<Integer> values = board.getValues(keys);
//            if (values.contains(2048)) {
//                return true;
//            }
//        }
//        return false;

        return board.hasValue(64);
    }
}
