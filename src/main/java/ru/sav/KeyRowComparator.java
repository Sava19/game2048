package ru.sav;

import java.util.Comparator;

public class KeyRowComparator implements Comparator<Key> {
    @Override
    public int compare(Key o1, Key o2) {
        return o1.getJ() - o2.getJ();
    }
}
