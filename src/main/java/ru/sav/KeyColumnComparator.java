package ru.sav;

import java.util.Comparator;

public class KeyColumnComparator implements Comparator<Key> {
    @Override
    public int compare(Key o1, Key o2) {
        return o1.getI() - o2.getI();
    }
}
