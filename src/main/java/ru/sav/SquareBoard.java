package ru.sav;

import java.util.ArrayList;
import java.util.List;

public class SquareBoard <V> extends Board <Key, V> {

    SquareBoard(int size) {
        super(size, size);
    }

    @Override
    void fillBoard(List<V> list) {
        //fill the bord with values from list one by one from left to right, from top to bottom

        if (list.size() > (this.width * this.height)) {
            throw new RuntimeException("The initial list is more than field size");
        }

        int i = 0;
        int j = 0;
        for (V value : list) {
            this.addItem(new Key(i, j), value);
            j++;
            if (j >= this.width) {
                i++;  //switch to a next row
                j = 0;
            }
        }
    }

    @Override
    List<Key> availableSpace() {
        List<Key> keysWithNullValue = new ArrayList<>(); //list of Keys that have null value

        for (Key key : board.keySet()) {
            V value = this.getValue(key);

            if (value == null) {
                keysWithNullValue.add(key);
            }
        }

        return keysWithNullValue;

    }

    @Override
    void addItem(Key key, V value) {
        this.board.put(key, value);
    }

    @Override
    Key getKey(int i, int j) {
        for (Key key : board.keySet()) {
            if (key.getI() == i && key.getJ() == j) {
                return key;
            }
        }

        return null;
    }

    @Override
    V getValue(Key key) {
        return board.get(key);
    }

    @Override
    List<Key> getColumn(int j) {
        List<Key> column = new ArrayList<>();
        for (Key key : board.keySet()) {
            if (key.getJ() == j) {
                column.add(key);
            }
        }

        column.sort(new KeyColumnComparator());
        return column;
    }

    @Override
    List<Key> getRow(int i) {
        List<Key> row = new ArrayList<>();
        for (Key key : board.keySet()) {
            if (key.getI() == i) {
                row.add(key);
            }
        }

        row.sort(new KeyRowComparator());
        return row;
    }

    @Override
    boolean hasValue(V value) {
        return board.values().contains(value);
    }

    @Override
    List<V> getValues(List<Key> keys) {
        List<V> values = new ArrayList<>();

        for (Key key : keys) {
            values.add(this.getValue(key));
        }

        return values;
    }
}
