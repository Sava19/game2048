package ru.sav;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import static java.util.Arrays.asList;
import static org.mockito.Mockito.*;



@ExtendWith(MockitoExtension.class)
public class Game2048Test {

    @Mock
    Board<Key, Integer> board;
    @Spy
    GameHelper helper;
    @InjectMocks
    Game2048 game;

    @Test
    void initTest() {
        doNothing().when(board).fillBoard(anyList());
        doReturn(asList(new Key(1, 1))).when(board).availableSpace();
        game.init();
        verify(board).fillBoard(anyList());

    }
}
