package ru.sav;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.*;

public class SquareBoardTest {

    @Test
    void fiilBoardTestMap() {
        SquareBoard sqBoard = new SquareBoard(3);
        List<Integer> initialValues = asList(2, null, null, 4, null, null, 2, null, 2048);
        sqBoard.fillBoard(initialValues);
        HashMap<Key, Integer> expectedMap = new HashMap<>();
        expectedMap.put(new Key(0, 0), 2);
        expectedMap.put(new Key(0, 1), null);
        expectedMap.put(new Key(0, 2), null);
        expectedMap.put(new Key(1, 0), 4);
        expectedMap.put(new Key(1, 1), null);
        expectedMap.put(new Key(1, 2), null);
        expectedMap.put(new Key(2, 0), 2);
        expectedMap.put(new Key(2, 1), null);
        expectedMap.put(new Key(2, 2), 2048);
        assertEquals(expectedMap, sqBoard.board);
    }

    @Test
    void fillBoardTestThrowException() {
        SquareBoard sqBoard = new SquareBoard(1);
        List<Integer> initialValues = asList(2, null, null, 4, null, null, 2, null, 2048);
        assertThrows(RuntimeException.class, () -> sqBoard.fillBoard(initialValues));
    }
}
