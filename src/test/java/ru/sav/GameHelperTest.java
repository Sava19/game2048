package ru.sav;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.*;

public class GameHelperTest {

    GameHelper helper;

    @BeforeEach
    void init() {
        helper = new GameHelper();
    }

    @Test
    void moveAndMergeEqualNulls() {
        List<Integer> expected = asList(null, null, null, null);
        List<Integer> actual = helper.moveAndMergeEqual(asList(null, null, null, null));
        assertEquals(expected, actual);
    }

    @Test
    void moveAndMergeEqualShiftOneValue() {
        List<Integer> expected = asList(1, 2, 3, null);
        List<Integer> actual = helper.moveAndMergeEqual(asList(1, 2, null, 3));
        assertEquals(expected, actual);
    }

    @Test
    void moveAndMergeEqual2_2_4_4() {
        List<Integer> expected = asList(4, 8, null, null);
        List<Integer> actual = helper.moveAndMergeEqual(asList(2, 2, 4, 4));
        assertEquals(expected, actual);
    }
}
